     I will be using completablefuture to asynchronise the crafting process, so through completablefuture we run tasks on a
separate thread from the main thread so that the task can be executed in parallel.With runAsync(), we run the specific task to
a different thread until it is finished and then the result will be notified to the main thread.

    It said that there are 2 files that can be modified, CraftItem and CraftServiceImpl. However I modified only CraftServiceImpl
(not including the bonus) because CraftItem only consists of the Item's name and does not include the crafting process, while the code
for creation of the items are in CraftServiceImpl. I am using asynchronous programming because the problem with the program before
edit is how long it takes to craft multiple of items, but by using asynchronous programming items can be crafted separately.

    The intended flow would be ,for making a single item, is first create the item on CraftItem class, runAsync adding
recipes to the item and then after all the recipes are added use thenRun to finish creating the item. thenRun() is used after
thenRunAsync() is finished it instantly runs the task given.



