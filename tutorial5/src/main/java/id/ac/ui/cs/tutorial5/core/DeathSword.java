package id.ac.ui.cs.tutorial5.core;

import id.ac.ui.cs.tutorial5.service.RandomizerService;

public class DeathSword extends Craftable {

    public DeathSword() {
        super("Death Sword", RandomizerService.getRandomCostValue() + 200);
    }

    @Override
    public String getDoneCraftMessage() {
        return "From the black they scream. \n " +
                "The shadow of the one who have left, screaming for the help from the other side. \n" +
                "The blood of the one fall in the battlefield are flowing through this black cursed sword.. \n";
    }
}
