package id.ac.ui.cs.tutorial3.service;

import id.ac.ui.cs.tutorial3.core.Synthesis;

import java.util.List;


public interface ManalithService {
	void addSynth(Synthesis synth);
	List<String> processRequest();
	List<Synthesis> getSynthesises();
}