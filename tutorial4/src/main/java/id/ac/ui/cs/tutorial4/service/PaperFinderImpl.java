package id.ac.ui.cs.tutorial4.service;

import java.util.Arrays;
import java.util.List;

import id.ac.ui.cs.tutorial4.dataclass.Document;
import id.ac.ui.cs.tutorial4.dataclass.Documents;
import id.ac.ui.cs.tutorial4.dataclass.KeyPhrasesResponseDocument;
import id.ac.ui.cs.tutorial4.dataclass.KeyPhrasesResult;
import id.ac.ui.cs.tutorial4.dataclass.PaperResult;
import id.ac.ui.cs.tutorial4.model.Conference;
import id.ac.ui.cs.tutorial4.service.supplier.ScienceDirectPaperFinder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * PaperFinderImpl
 */
@Service
public class PaperFinderImpl implements PaperFinderService {



    @Override
    public PaperResult findPaperFromConference(Conference conference) {
        String query = getQueriesFromConference(conference);

        // Setup the documents wrapper
        Document docWrapper = new Document("", "en", query);
        Documents docs = new Documents();
        docs.addDocument(docWrapper);


        List<String> keyPhrases = Arrays.asList(docs.toString().split(" "));
        PaperResult pResult = ScienceDirectPaperFinder.getInstance().findPapers(condenseString(keyPhrases));
        return pResult;
    }

    private String getQueriesFromConference(Conference conference) {
        return conference.getNama();
    }

    private String condenseString(List<String> strings) {
        StringBuilder sb = new StringBuilder();
        for (String s : strings) {
            sb.append(s);
            sb.append(" ");
        }
        return sb.toString();
    }

}